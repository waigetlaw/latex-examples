\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\contentsline {section}{List of Tables}{iii}{section*.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{List of Figures}{iv}{section*.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{List of Listings}{v}{section*.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{List of Equations}{vi}{section*.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Tables}{1}{section.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.1}Another Table}{1}{subsection.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}Figures}{2}{section.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3}Code Snippets}{3}{section.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4}Citing}{4}{section.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5}Maths}{5}{section.5}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1}Basic}{5}{subsection.5.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2}Advanced}{5}{subsection.5.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{References}{6}{equation.5.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{Appendices}{I}{section*.5}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {A}Abelian Sandpiles}{I}{appendix.A}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {B}Identity Sandpile}{II}{appendix.B}%
